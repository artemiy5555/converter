import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class ControllerTest {

    @Test
    void test_converterOfSec(){
        BigDecimal sec=new BigDecimal(1);
        assertEquals("0.01667",Controller.converterOfSec("Min",sec));
        assertEquals("0.00028",Controller.converterOfSec("Hour",sec));
        assertEquals("0.00002",Controller.converterOfSec("Day",sec));
    }
    @Test
    void test_converterInSec(){
        BigDecimal time=new BigDecimal(1);
        assertEquals("60",Controller.converterInSec("Min",time));
        assertEquals("3600",Controller.converterInSec("Hour",time));
        assertEquals("86400",Controller.converterInSec("Day",time));
    }
    @Test
    void test_converterOfKg(){
        BigDecimal kg=new BigDecimal(1);
        assertEquals("0.001",Controller.converterOfKg("g",kg));
        assertEquals("0.01",Controller.converterOfKg("c",kg));
    }
    @Test
    void test_converterInKg(){
        BigDecimal weight=new BigDecimal(0.001);
        assertEquals("1",Controller.converterInKg("g",weight));
        assertEquals("0.1",Controller.converterInKg("c",weight));
    }
    @Test
    void test_converterOfL(){
        BigDecimal l=new BigDecimal(1);
        assertEquals("0.001",Controller.converterOfL("m^3",l));
        assertEquals("0.21979",Controller.converterOfL("gallon",l));

    }
    @Test
    void test_converterInL(){
        BigDecimal volume=new BigDecimal(0.001);
        assertEquals("1",Controller.converterInL("m^3",volume));
        assertEquals("4.55",Controller.converterInL("gallon",volume));

    }
    @Test
    void test_converterOfM(){
        BigDecimal m=new BigDecimal(1);
        assertEquals("0.001",Controller.converterOfM("km",m));
        assertEquals("0.00063",Controller.converterOfM("mile",m));

    }
    @Test
    void test_converterInM(){
        BigDecimal len=new BigDecimal(1);
        assertEquals("1000",Controller.converterInM("km",len));
        assertEquals("1609.34401",Controller.converterInM("mile",len));
    }
    @Test
    void test_converterOfC(){
        BigDecimal c=new BigDecimal(1);
        assertEquals("275.1",Controller.converterOfM("K",c));
        assertEquals("34.8",Controller.converterOfM("F",c));

    }
    @Test
    void test_converterInC(){
        BigDecimal temp=new BigDecimal(1);
        assertEquals("-273.1",Controller.converterInM("K",temp));
        assertEquals("-32.79999",Controller.converterInM("F",temp));
    }

}
