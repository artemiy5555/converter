import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;

import java.math.BigDecimal;

import static java.math.BigDecimal.ROUND_CEILING;


public class Controller {

    @FXML
    private TextField fildSec;

    @FXML
    private TextField fildKg;

    @FXML
    private TextField fildL;

    @FXML
    private TextField fildM;

    @FXML
    private TextField fildC;

    @FXML
    private TextField fildTime;

    @FXML
    private TextField fildWeight;

    @FXML
    private TextField fildVolume;

    @FXML
    private TextField fildLength;

    @FXML
    private TextField fildTemperature;

    @FXML
    Spinner<String> spinnerTime = new Spinner<>();

    ObservableList<String> time = FXCollections.observableArrayList(//
            "Min", "Hour", "Day","Week","Month","Astronomical Year","Third");

    @FXML
    Spinner<String> spinnerWeight = new Spinner<>();

    ObservableList<String> weight = FXCollections.observableArrayList(//
            "g", "c", "carat","eng pound","pound","stone","rus pound");

    @FXML
    Spinner<String> spinnerVolume = new Spinner<>();

    ObservableList<String> volume = FXCollections.observableArrayList(//
            "m^3", "gallon", "pint","quart","barrel","cubic foot","cubic inch");

    @FXML
    Spinner<String> spinnerLenght = new Spinner<>();

    ObservableList<String> lenght = FXCollections.observableArrayList(//
            "km", "mile", "nautical mile", "cable","league","foot","yard");

    @FXML
    Spinner<String> spinnerTemperature = new Spinner<>();

    ObservableList<String> temperature = FXCollections.observableArrayList(//
            "K", "F", "Re", "Ro","Ra","N","D");


    public void initialize(){
        SpinnerValueFactory<String> valueTime = new SpinnerValueFactory.ListSpinnerValueFactory<>(time);
        valueTime.setValue("Min");
        spinnerTime.setValueFactory(valueTime);

        SpinnerValueFactory<String> valueWeight = new SpinnerValueFactory.ListSpinnerValueFactory<>(weight);
        valueWeight.setValue("g");
        spinnerWeight.setValueFactory(valueWeight);

        SpinnerValueFactory<String> valueVolume = new SpinnerValueFactory.ListSpinnerValueFactory<>(volume);
        valueVolume.setValue("m^3");
        spinnerVolume.setValueFactory(valueVolume);

        SpinnerValueFactory<String> valueLenght = new SpinnerValueFactory.ListSpinnerValueFactory<>(lenght);
        valueLenght.setValue("km");
        spinnerLenght.setValueFactory(valueLenght);

        SpinnerValueFactory<String> valueTemperature = new SpinnerValueFactory.ListSpinnerValueFactory<>(temperature);
        valueTemperature.setValue("K");
        spinnerTemperature.setValueFactory(valueTemperature);
    }

    @FXML
    private void inSec(){
        try {
            fildSec.setText(converterInSec(spinnerTime.getValue(), BigDecimal.valueOf(Double.parseDouble(fildTime.getText()))));
        }catch (NumberFormatException  e){fildSec.setText("введите корректные данные");}
    }

    @FXML
    private void ofSec() {
        try {
            fildTime.setText(converterOfSec(spinnerTime.getValue(), BigDecimal.valueOf(Double.parseDouble(fildSec.getText()))));
        }catch (NumberFormatException  e){fildTime.setText("введите корректные данные");}
    }
    @FXML
    private void inKg(){
        try {
            fildKg.setText(converterInKg(spinnerWeight.getValue(), BigDecimal.valueOf(Double.parseDouble(fildWeight.getText()))));
        }catch (NumberFormatException  e){fildKg.setText("введите корректные данные");}
    }

    @FXML
    private void ofKg(){
        try {
            fildWeight.setText(converterOfKg(spinnerWeight.getValue(), BigDecimal.valueOf(Double.parseDouble(fildKg.getText()))));
        }catch (NumberFormatException  e){fildWeight.setText("введите корректные данные");}
    }

    @FXML
    private void inL(){
        try {
            fildL.setText(converterInL(spinnerVolume.getValue(), BigDecimal.valueOf(Double.parseDouble(fildVolume.getText()))));
        }catch (NumberFormatException  e){fildL.setText("введите корректные данные");}
    }

    @FXML
    private void ofL() {
        try {
            fildVolume.setText(converterOfL(spinnerVolume.getValue(), BigDecimal.valueOf(Double.parseDouble(fildL.getText()))));
        }catch (NumberFormatException  e){fildVolume.setText("введите корректные данные");}
    }
    @FXML
    private void inM(){
        try {
            fildM.setText(converterInM(spinnerLenght.getValue(), BigDecimal.valueOf(Double.parseDouble(fildLength.getText()))));
        }catch (NumberFormatException  e){fildM.setText("введите корректные данные");}
    }

    @FXML
    private void ofM(){
        try {
        fildLength.setText(converterOfM(spinnerLenght.getValue(),BigDecimal.valueOf(Double.parseDouble(fildM.getText()))));
        }catch (NumberFormatException  e){fildLength.setText("введите корректные данные");}
    }

    @FXML
    private void inC(){
        try {
        fildC.setText(converterInC(spinnerTemperature.getValue(),BigDecimal.valueOf(Double.parseDouble(fildTemperature.getText()))));
        }catch (NumberFormatException  e){fildC.setText("введите корректные данные");}
    }

    @FXML
    private void ofC(){
        try {
        fildTemperature.setText(converterOfC(spinnerTemperature.getValue(),BigDecimal.valueOf(Double.parseDouble(fildC.getText()))));
        }catch (NumberFormatException  e){fildTemperature.setText("введите корректные данные");}
    }

    public static String converterOfSec(String covertTo, BigDecimal sec){

        switch (covertTo){
            case "Min":
                return sec.divide(new BigDecimal(60),5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Hour":
                return sec.divide(new BigDecimal(3600),5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Day":
                return sec.divide(new BigDecimal(86400),5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Week":
                return sec.divide(new BigDecimal(604800),5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Month":
                return sec.divide(new BigDecimal(2678400),5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Astronomical Year":
                return sec.divide(new BigDecimal(31536000),5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Third":
                return sec.divide(new BigDecimal(31536000*3),5, ROUND_CEILING).stripTrailingZeros().toString();
        }
        return sec.toString();
    }


    public static String converterInSec(String covertTo, BigDecimal time){

            switch (covertTo){
                case "Min":
                    return time.multiply(new BigDecimal(60)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "Hour":
                    return time.multiply(new BigDecimal(3600)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "Day":
                    return time.multiply(new BigDecimal(86400)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "Week":
                    return time.multiply(new BigDecimal(604800)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "Month":
                    return time.multiply(new BigDecimal(2678400)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "Astronomical Year":
                    return time.multiply(new BigDecimal(31536000)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "Third":
                    return time.multiply(new BigDecimal(31536000*3)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            }
        return null;
    }
    public static String converterOfKg(String covertTo, BigDecimal kg){

            switch (covertTo){
                case "g":
                    return kg.divide(new BigDecimal(0.001),10, ROUND_CEILING).stripTrailingZeros().toString();
                case "c":
                    return kg.divide(new BigDecimal(100),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "carat":
                    return kg.multiply(new BigDecimal(0.45359237)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "eng pound":
                    return kg.multiply(new BigDecimal(0.373)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "pound":
                    return kg.divide(new BigDecimal(2.21),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "stone":
                    return kg.divide(new BigDecimal(6.35029318),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "rus pound":
                    return kg.multiply(new BigDecimal(0.4914)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            }
        return null;
    }

    public static String converterInKg(String covertTo, BigDecimal weight){

            switch (covertTo){
                case "g":
                    return weight.divide(new BigDecimal(0.001),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "c":
                    return weight.multiply(new BigDecimal(100)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "carat":
                    return weight.divide(new BigDecimal(0.45359237),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "eng pound":
                    return weight.divide(new BigDecimal(0.373),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "pound":
                    return weight.multiply(new BigDecimal( 2.21)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "stone":
                    return weight.multiply(new BigDecimal( 6.35029318)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "rus pound":
                    return weight.divide(new BigDecimal(0.4914),5, ROUND_CEILING).stripTrailingZeros().toString();
            }
        return null;
    }

    public static String converterOfL(String covertTo, BigDecimal l){

            switch (covertTo){
                case "m^3":
                    return l.divide(new BigDecimal(1000.0000000001),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "gallon":
                    return l.divide(new BigDecimal( 4.55),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "pint":
                    return l.multiply(new BigDecimal(0.57)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "quart":
                    return l.multiply(new BigDecimal( 0.9463)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "barrel":
                    return l.divide(new BigDecimal( 119.2),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "cubic foot":
                    return l.divide(new BigDecimal(  28.32),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "cubic inch":
                    return l.divide(new BigDecimal(  61.024),5, ROUND_CEILING).stripTrailingZeros().toString() ;
            }
        return null;
    }

    public static String converterInL(String covertTo, BigDecimal volume){

            switch (covertTo){
                case "m^3":
                    return volume.multiply(new BigDecimal(1000.00000001)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "gallon":
                    return volume.multiply(new BigDecimal(4.55)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "pint":
                    return volume.divide(new BigDecimal(0.57),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "quart":
                    return volume.divide(new BigDecimal(0.9463),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "barrel":
                    return volume.multiply(new BigDecimal(119.2)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "cubic foot":
                    return volume.multiply(new BigDecimal(28.32)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "cubic inch":
                    return volume.multiply(new BigDecimal(61.024)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            }
        return null;
    }

    public static String converterOfM(String covertTo, BigDecimal m){

            switch (covertTo){
                case "km":
                    return m.divide(new BigDecimal(1000),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "mile":
                    return m.multiply(new BigDecimal(0.000621371192)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "nautical mile":
                    return m.multiply(new BigDecimal(0.000539956803)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "cable":
                    return m.divide(new BigDecimal(185.2),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "league":
                    return m.multiply(new BigDecimal(0.000179985601)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "foot":
                    return m.multiply(new BigDecimal(3.2808399)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "yard":
                    return m.multiply(new BigDecimal(1.0936133)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            }
        return null;
    }

    public static String converterInM(String covertTo, BigDecimal len){

            switch (covertTo){
                case "km":
                    return len.multiply(new BigDecimal(1000)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "mile":
                    return len.divide(new BigDecimal(0.000621371192),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "nautical mile":
                    return len.divide(new BigDecimal(0.000539956803),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "cable":
                    return len.multiply(new BigDecimal(185.2)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
                case "league":
                    return len.divide(new BigDecimal(0.000179985601),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "foot":
                    return len.divide(new BigDecimal(3.2808399),5, ROUND_CEILING).stripTrailingZeros().toString();
                case "yard":
                    return len.divide(new BigDecimal(1.0936133),5, ROUND_CEILING).stripTrailingZeros().toString();
            }
        return null;
    }

    public static String converterOfC(String covertTo, BigDecimal c){

        switch (covertTo){
            case "K":
                return c.add(new BigDecimal(274.1)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            case "F":
                return c.add(new BigDecimal(33.8)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Re":
                return c.multiply(new BigDecimal(0.8)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Ro":
                return c.multiply(new BigDecimal(0.525)).add(new BigDecimal(7.5)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Ra":
                return c.add(new BigDecimal(491.67)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            case "N":
                return c.multiply(new BigDecimal(0.33)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            case "D":
                return c.multiply(new BigDecimal(1.0936133)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
        }
        return null;
    }

    public static String converterInC(String covertTo, BigDecimal temp){

        switch (covertTo){
            case "K":
                return temp.subtract(new BigDecimal(274.1)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            case "F":
                return temp.subtract(new BigDecimal(33.8)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Re":
                return temp.divide(new BigDecimal(0.8),5, ROUND_CEILING).stripTrailingZeros().toString();
            case "Ro":
                return temp.divide(new BigDecimal(0.525),5, ROUND_CEILING).subtract(new BigDecimal(7.5)).stripTrailingZeros().toString();
            case "Ra":
                return temp.add(new BigDecimal(491.67)).setScale(5, ROUND_CEILING).stripTrailingZeros().toString();
            case "N":
                return temp.divide(new BigDecimal(0.33),5, ROUND_CEILING).stripTrailingZeros().toString();
            case "D":
                return temp.divide(new BigDecimal(1.0936133),5, ROUND_CEILING).stripTrailingZeros().toString();
        }
        return null;
    }
}
